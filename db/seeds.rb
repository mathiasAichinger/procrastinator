# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Alternative.create(title: "Dein Zimmer gehört mal wieder aufgeräumt! 🗑")
Alternative.create(title: "Du hast bestimmt Hunger. Koch dir was! 🍳")
Alternative.create(title: "Draußen ist es so schön. Geh frische Luft schnappen! ☀️")
Alternative.create(title: "Die Serie ist so spannend! Eine Folge geht sich noch aus! 📺")
Alternative.create(title: "Dein Freund ist gerade online gekommen. Spiel eine Runde mit ihm! 🎮")
Alternative.create(title: "Nach einem Powernap gehts gleich viel besser! 🛌")