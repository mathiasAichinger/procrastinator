class BotMessagesController < ApplicationController

  def get_alternative
    offset = rand(Alternative.count)
    rand_alternative = Alternative.offset(offset).first

    messages = {
        "messages": [
            { "text": rand_alternative.title }
        ]
    }

    render json: messages, status: :ok
  end
end
