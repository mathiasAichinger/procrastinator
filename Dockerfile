# Base our image on an official, minimal image of our preferred Ruby
FROM ruby:2.5.1-slim

# Install essential Linux packages
RUN apt-get update -qq && apt-get install -y build-essential  git libpq-dev postgresql-client nano

# Define where our application will live inside the image
ENV RAILS_ROOT /var/www/procrastinator

# Create application home. App server will need the pids dir so just create everything in one shot
RUN mkdir -p $RAILS_ROOT/tmp/pids

# Set our working directory inside the image
WORKDIR $RAILS_ROOT

COPY Gemfile Gemfile

COPY Gemfile.lock Gemfile.lock

# Prevent bundler warnings; ensure that the bundler version executed is >= that which created Gemfile.lock
RUN gem install bundler

# Finish establishing our Ruby enviornment
RUN bundle install --jobs 4

# Copy the Rails application into place
COPY . .

CMD [ "config/app.sh" ]