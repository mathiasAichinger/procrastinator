require 'rails_helper'

RSpec.describe "Alternatives", type: :request do
  describe "GET /alternatives/1" do
    it "should only have a id, title, created_at and updated_at attribute. additional attributes will cause the Android app to crash" do
      a = Alternative.create(title: "Test", id: 1)

      get '/alternatives/1'

      a.destroy

      expect(response.status).to eq 200

      body_hash = JSON.parse(response.body)

      expect(body_hash.keys).to eq (["id", "title", "created_at", "updated_at"])
    end
  end
end

