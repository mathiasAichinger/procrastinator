#!/usr/bin/env bash

sleep 10
rake db:create
rake db:migrate
rake db:seed

bundle exec puma -C config/puma.rb