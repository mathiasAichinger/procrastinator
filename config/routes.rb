Rails.application.routes.draw do
  resources :alternatives
  get 'get_alternative', to: 'bot_messages#get_alternative'
end
